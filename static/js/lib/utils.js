function sendRequest(request_data) {
    let url = `http://78.155.219.128:8888/${request_data.path}/`;

    if (request_data.id) {
        url = `${url}${request_data.id}/`;
    }

    let type = 'GET';
    if (request_data.type) {
        type = request_data.type
    }

    return $.ajax({
        type: type,
        url: url,
        data: request_data.data,
        headers: {
            "Authorization": `Token ${request_data.token}`,
        }
    });
}

const serverInfo = {
    host: 'http://78.155.219.128:8888',

};

function deleteAlert(function_after) {

    swal({
        title: "Вы уверены ?",
        text: "Вы не сможете восстановить удаленные данные",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Уверен",
        cancelButtonText: "Нет, я передумал)",
        closeOnConfirm: false
    }, function () {
        function_after();
        swal("Удалено!", "Успешно удалено.", "success");
    });
}

export default { sendRequest, serverInfo, deleteAlert }