import Reflux from 'reflux'

export default Reflux.createActions([
    'changeTaskStatus',
    'changeTask',
    'deleteTask',
    'addTask',
    'saveNewTask'
]);