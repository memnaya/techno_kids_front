import Reflux from 'reflux'

export default Reflux.createActions([
    'addMember',
    'changeMember',
    'createMember',
    'deleteMember'
]);