import Reflux from 'reflux'

export default Reflux.createActions([
    'addPrize',
    'createPrize',
    'editPrize',
    'deletePrize'
]);