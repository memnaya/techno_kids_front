import Reflux from 'reflux'

export default Reflux.createActions([
    'addRole',
    'saveNewRole'
]);