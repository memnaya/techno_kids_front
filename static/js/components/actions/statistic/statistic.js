import Reflux from 'reflux'

export default Reflux.createActions([
    'addSin',
    'saveSin'
]);