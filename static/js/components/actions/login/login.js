import Reflux from 'reflux'

export default Reflux.createActions([
    'login',
    'logout',
    'registration'
]);