import React from 'react'
import Reflux from 'reflux';
import {Link} from 'react-router'

import LoginStore from './../../stores/login';

import Utils from  './../../../lib/utils'

require('./../../../../css/content/__statistic/icons.css');

export default class ChildStatistic extends Reflux.Component {
    constructor() {
        super();
        this.store = LoginStore;
    }

    componentDidMount() {
        let tasks_array = [];
        this.props.data.tasks.forEach((el)=> {
            tasks_array.push(
                <li className="collection-item"><div>{el.title}<a className="secondary-content good"><i className="material-icons">done</i></a></div></li>
            )
        });
        let sins_array = [];
        this.props.data.sins.forEach((el)=> {
            sins_array.push(
                <li className="collection-item"><div>{el.title}<a className="secondary-content bad"><i className="material-icons">not_interested</i></a></div></li>
            )
        });
        this.setState({
            tasks: tasks_array,
            sins: sins_array
        });
    }

    render() {
        return (
            <ul className="collection with-header">
                {this.state.sins}
                {this.state.tasks}
            </ul>
        )
    }
}