import React from 'react'
import Reflux from 'reflux';
import {Link} from 'react-router'

import LoginStore from './../../stores/login';
import StatisticStore from './../../stores/statistic';

import Utils from  './../../../lib/utils'

require('./../../../../css/content/__statistic/icons.css');

export default class ParentStatistic extends Reflux.Component {
    constructor() {
        super();
        this.stores = [LoginStore, StatisticStore];
    }

    componentDidMount() {
        let tasks_array = [];
        this.props.data.tasks.forEach((el)=>{
            el.tasks.forEach((task)=> {
                tasks_array.push(
                    <li className="collection-item">
                        <div>{el.name} - {task.title}<a className="secondary-content good"><i className="material-icons">done</i></a>
                        </div>
                    </li>
                );
            });
        });

        let sins_array = [];
        this.props.data.sins.forEach((el)=>{
            el.sins.forEach((sin)=>{
                sins_array.push(
                    <li className="collection-item"><div>{el.name} - {sin.title}<a className="secondary-content bad"><i className="material-icons">not_interested</i></a></div></li>
                )
            });
        });
        this.setState({
            tasks: tasks_array,
            sins: sins_array
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.new_sin_row) {
            nextState.sins.unshift(nextState.new_sin_row);
            nextState.new_sin = 'empty';
        }
        return true;
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.new_sin === 'empty') {
            this.setState({
                new_sin_row: undefined,
                new_sin: undefined
            });
        }
    }

    render() {
        return (
            <ul className="collection with-header">
                {this.state.new_sin}
                {this.state.sins}
                {this.state.tasks}
            </ul>
    )
    }
}