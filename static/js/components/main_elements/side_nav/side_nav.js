import React from 'react'
import Reflux from 'reflux';
import {Link} from 'react-router'

import LoginStore from './../../stores/login';
import LoginAction from './../../actions/login/login';

import ParentNav from './parent'
import ChildNav from './child'

import Utils from  './../../../lib/utils'

require('./../../../../css/side_nav/side_nav.css');

export default class SideNav extends Reflux.Component {
    constructor() {
        super();
        this.store = LoginStore;
    }

    componentDidMount() {
        let nav;
        if (this.state.isParent) {
            nav = <ParentNav/>;
        } else {
            nav = <ChildNav/>;
        }

        this.setState({
            nav: nav
        });
        $('.button-collapse').sideNav();
    }

    render() {
        return (
            <div>
                <ul id="slide-out" className="side-nav fixed">
                    <div className="side_user">
                        <div>
                            <img className="circle"
                                 src="https://pp.userapi.com/c636630/v636630221/145da/jO3-XS4Nah8.jpg"/>
                        </div>
                        <div className="side_user_info">
                            <p
                                className="white-text name">{`${this.state.user.user.first_name} ${this.state.user.user.last_name}`}</p>
                            <p className="white-text name">
                                {`Баланс: ${this.state.user.balance} баллов`}
                            </p>
                        </div>

                    </div>
                    {this.state.nav}
                    <li className="quit">
                        <a className="waves-effect" onClick={LoginAction.logout}>Выйти</a>
                    </li>
                </ul>
                <a href="#" data-activates="slide-out" className="button-collapse"><i
                    className="material-icons">menu</i></a>
            </div>
        )
    }
}