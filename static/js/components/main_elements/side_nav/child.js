import React from 'react'
import Reflux from 'reflux';
import {Link} from 'react-router'

import Utils from  './../../../lib/utils'

export default class ChildNav extends Reflux.Component {
    render() {
        return (
            <div>
                <li><Link className="waves-effect" to="child">Задачи</Link></li>
                <li><Link className="waves-effect" to="prises">Призы</Link></li>
                <li><Link className="waves-effect" to="statistic">Статистика</Link></li>
                {/*<li><Link className="waves-effect" to="diary">Мой дневник</Link></li>*/}
            </div>
        )
    }
}