import React from 'react'
import { Link } from 'react-router'

export default class Mark extends React.Component {
    render() {
        return (
            <li className="collection-item">{this.props.data.name} - {this.props.data.mark}</li>
        )
    }
}