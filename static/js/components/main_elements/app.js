import React from "react";
import Reflux from 'reflux'

//main components
import SideNav from './side_nav/side_nav'
import LoginStore from './.././stores/login'
import Footer from './footer/footer'

// libs
// import 'materialize-css/bin/materialize'
import 'sweetalert/dist/sweetalert.css';
import 'sweetalert/dist/sweetalert.min';
// main styles
require('./../../../css/lib/materialize.min.css');
require('./../../../css/main.css');

// bam styles
require('./../../../css/content/content.css');
require('./../../../css/footer/footer.css');

export default class App extends Reflux.Component {
    constructor() {
        super();
        this.store = LoginStore;
    }

    render() {
        return (
            <div>
                { window.location.pathname !== "/login" && this.state.isParent !== undefined ? <SideNav/> : null}
                <main className="content">
                    {this.props.children}
                </main>
                {/*<Footer/>*/}
            </div>
        );
    }
}