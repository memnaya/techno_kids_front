import React from "react";
import Reflux from 'reflux';

import TaskStore from "./../../stores/task"
import TaskAction from './../../actions/task/task';

import Select from './../select/select'

import EditCardContent from './task_content/edit';
import CreateCardContent from './task_content/create';

import CookieManager from  './../../../lib/cookie'
import Utils from  './../../../lib/utils'

require('./../../../../css/content/__tasks__card/card.css');

export default class Card extends Reflux.Component {
    constructor() {
        super();
        this.store = TaskStore;
        this.url = 'tasktype'
    }

    handleCardStyle(data, button) {
        if (data.status === 0) {

            button.removeClass('red lighten-2');
            button.find('i').text('done');
            button.parent().parent().removeClass('teal');

        } else if (data.status === 1) {

            button.parent().parent().parent().find('.card__alert__button').show();

        } else if (data.status === 2) {

            button.addClass('red lighten-2');
            button.find('i').text('replay');
            button.parent().parent().addClass('teal');
            button.parent().parent().parent().find('.card__alert__button').hide();

        }
    }

    styleCard() {
        let button = $(`button[data-id=${this.props.id}]`).first();
        if (this.props.status == 0) {
            this.handleCardStyle(this.props, button);
        } else if (this.props.status == 1) {
            this.handleCardStyle(this.props, button);
        } else if (this.props.status == 2) {
            this.handleCardStyle(this.props, button);
        }
    }

    chooseButton() {
        let card_content = <EditCardContent
            title={this.props.title}
            taskDecription={this.props.taskDecription}
            revard={this.props.revard}
            id={this.props.id}
        />;

        if (this.props.emptyTask) {
            card_content = <CreateCardContent
                title={'Введите заголовок'}
                taskDecription={'Введите описание'}
            />;
        }

        this.setState({
            card_content: card_content
        });
    }

    componentDidMount() {
        this.styleCard();
        this.chooseButton();
        let picture = Utils.serverInfo.host + '/media/' + this.props.picture;
        this.setState({
            picture: picture
        });
        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: this.url,
            token: token,

        }).success((data)=> {
            this.setState({
                select: <Select selected={this.props.typeId} options={data}/>
            });
        });
    }

    render() {
        return (
            <div className="col s12 m6 card_position">
                <a className="card__alert__button btn-floating btn-large red">
                    <i className="material-icons">new_releases</i>
                </a>
                <div className="card sticky-action" style={{overflow: 'hidden'}}>
                    <div className="card-image waves-effect waves-block waves-light">
                        <img className="activator" src='http://lorempixel.com/400/600/'/>
                        {/*<img className="activator" src={this.state.picture}/>*/}
                    </div>
                    <div className="card-content">
                        <span className="card-title activator grey-text text-darken-4">
                            {this.props.taskName}
                            <i className="material-icons right">more_vert</i>
                        </span>
                    </div>

                    <div className="card-action">
                        <button onClick={TaskAction.changeTaskStatus} className="waves-effect waves-light btn"
                                data-id={this.props.id}>
                            <i className="material-icons">done</i>
                        </button>
                        <button onClick={TaskAction.deleteTask}
                                className="waves-effect waves-light btn right red darken-3"
                                data-id={this.props.id}>
                            <i className="material-icons">delete</i>
                        </button>
                    </div>

                    <div className="card-reveal" style={{display: 'none', transform: "translateY(0px)"}}>
                        <span className="card-title grey-text text-darken-4">
                            {this.props.taskName}
                            <i className="material-icons right">close</i>
                        </span>
                        <div>
                            <div className="input-field">
                                {this.state.select}
                            </div>
                            {this.state.card_content}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}