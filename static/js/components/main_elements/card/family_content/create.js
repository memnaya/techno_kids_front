import React from 'react';
import Reflux from 'reflux';

import FamilyStore from "./../../../stores/family";
import FamilyAction from './../../../actions/family/family';

import Select from './../../select/select'

import CookieManager from  './../../../../lib/cookie'
import Utils from  './../../../../lib/utils'

export default class CreateCardContent extends Reflux.Component {
    constructor() {
        super();
        this.store = FamilyStore;
        this.url = 'roles';
    }

    componentDidMount() {
        $('.datepicker').pickadate({
            container: ".container",
            selectMonths: true,
            selectYears: 15,
            format: 'yyyy-mm-dd'
        });

        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: this.url,
            token: token,

        }).success((data)=> {
            let male_data = [{
                name: " М",
                id: 1
            }, {
                name: "Ж",
                id: 0
            }];

            let selects = [
                <Select change="role" selected={0} options={data}/>,
                <Select change="gender" selected={0} options={male_data}/>
            ];

            this.setState({
                selects: selects
            });
        });
    }

    render() {
        return (
            <div>
                <p data-change="email" contentEditable="true">{this.props.email}</p>
                <p data-change="first_name" contentEditable="true">{this.props.first_name}</p>
                <p data-change="last_name" contentEditable="true">{this.props.last_name}</p>
                <p data-change="password" contentEditable="true">{this.props.password}</p>
                <div className="input-field">
                    <input data-change="birth_date" type="date" className="datepicker" />
                </div>
                <div className="input-field">
                    {this.state.selects}
                </div>
                <button className="waves-effect waves-light btn"
                        onClick={FamilyAction.createMember}>Создать
                </button>
            </div>
        )
    }
}