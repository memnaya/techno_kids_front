import React from 'react';
import Reflux from 'reflux';

import FamilyStore from "./../../../stores/family";
import FamilyAction from './../../../actions/family/family';

export default class EditCardContent extends Reflux.Component {
    constructor() {
        super();
        this.store = FamilyStore;
    }

    componentDidMount() {
        $(`.datepicker[data-picker=${this.props.id}]`).pickadate({
            container: ".container",
            selectMonths: true,
            selectYears: 15,
            format: 'yyyy-mm-dd'
        }).pickadate('picker').set('select', this.props.birth_date.split('-'));
    }

    render() {
        return (
            <div>
                <p >{this.props.email}</p>
                <p data-change="first_name" contentEditable="true">{this.props.first_name}</p>
                <p data-change="last_name" contentEditable="true">{this.props.last_name}</p>
                <div className="input-field">
                    <input data-picker={this.props.id} placeholder="Выберите дату рождения" data-change="birth_date" type="date" className="datepicker"/>
                </div>
                <button data-id={this.props.id} className="waves-effect waves-light btn"
                        onClick={FamilyAction.changeMember}>Изменить
                </button>
            </div>
        )
    }
}