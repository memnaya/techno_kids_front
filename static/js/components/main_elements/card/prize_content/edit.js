import React from 'react';
import Reflux from 'reflux';

import PrizeStore from "./../../../stores/prize";
import LoginStore from "./../../../stores/login";
import PrizeAction from './../../../actions/prize/prize';

export default class EditCardContent extends Reflux.Component {
    constructor() {
        super();
        this.stores = [PrizeStore, LoginStore];
    }

    componentDidMount() {
        if (this.state.isParent) {
            this.setState({
                change_button: <button data-id={this.props.id} className="waves-effect waves-light btn"
                                onClick={PrizeAction.editPrize}>Изменить
                </button>
            });
        }
    }

    render() {
        return (
            <div>
                <p data-change="name" contentEditable={this.state.isParent? "true": "false"}>{this.props.name}</p>
                <p data-change="description" contentEditable={this.state.isParent? "true": "false"}>{this.props.description}</p>
                <p> Стоимость:
                    <span data-change="cost" contentEditable={this.state.isParent? "true": "false"}>
                        {`   ${this.props.cost}`}
                </span>
                </p>
                {this.state.change_button}
            </div>
        )
    }
}