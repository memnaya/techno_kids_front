import React from 'react';
import Reflux from 'reflux';

import PrizeStore from "./../../../stores/prize";
import PrizeAction from './../../../actions/prize/prize';

export default class CreateCardContent extends Reflux.Component {
    constructor() {
        super();
        this.store = PrizeStore;
    }

    render() {
        return (
            <div>
                <p data-change="name" contentEditable="true">{this.props.name}</p>
                <p data-change="description" contentEditable="true">{this.props.description}</p>
                <p> Стоимость:
                    <span data-change="cost" contentEditable="true">
                        {`     `}
                </span>
                </p>
                <button className="waves-effect waves-light btn"
                        onClick={PrizeAction.createPrize}>Создать
                </button>
            </div>
        )
    }
}