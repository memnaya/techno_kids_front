import React from "react";
import Reflux from 'reflux';

import FamilyStore from "./../../stores/family"
import FamilyAction from './../../actions/family/family';

import EditCardContent from './family_content/edit';
import CreateCardContent from './family_content/create';

import Utils from  './../../../lib/utils'

require('./../../../../css/content/__tasks__card/card.css');

export default class Card extends Reflux.Component {
    constructor() {
        super();
        this.store = FamilyStore;
        this.url = 'family'
    }

    chooseButton() {
        let card_content = <EditCardContent
            email={this.props.email}
            first_name={this.props.first_name}
            last_name={this.props.last_name}
            id={this.props.id}
            birth_date={this.props.birth_date}
        />;

        if (this.props.emptyTask) {
            card_content = <CreateCardContent
                email={'Введите email'}
                first_name={'Введите имя'}
                last_name={'Введите фамилию'}
                password={'Введите пароль'}
            />;
        }

        this.setState({
            card_content: card_content
        });
    }

    componentDidMount() {
        this.chooseButton();
    }

    render() {
        return (
            <div className="col s12 m6 card_position">
                <div className="card sticky-action" style={{overflow: 'hidden'}}>
                    <div className="card-image waves-effect waves-block waves-light">
                        <img className="activator" src='http://lorempixel.com/400/600/'/>
                    </div>
                    <div className="card-content">
                        <span className="card-title activator grey-text text-darken-4">
                            { this.props.title ? this.props.title : `${this.props.first_name} ${this.props.last_name}`}
                            <i className="material-icons right">more_vert</i>
                        </span>
                    </div>

                    <div className="card-action">
                        <button onClick={FamilyAction.deleteMember}
                                className="waves-effect waves-light btn red darken-3"
                                data-id={this.props.id}>
                            <i className="material-icons">delete</i>
                        </button>
                    </div>

                    <div className="card-reveal" style={{display: 'none', transform: "translateY(0px)"}}>
                        <span className="card-title grey-text text-darken-4">
                            {this.props.first_name} {this.props.last_name}
                            <i className="material-icons right">close</i>
                        </span>
                        {this.state.card_content}
                    </div>
                </div>
            </div>
        )
    }
}