import React from "react";
import Reflux from 'reflux';

import PrizeStore from "./../../stores/prize"
import LoginStore from "./../../stores/login"
import PrizeAction from './../../actions/prize/prize';

import EditCardContent from './prize_content/edit';
import CreateCardContent from './prize_content/create';

import Utils from  './../../../lib/utils'

require('./../../../../css/content/__tasks__card/card.css');

export default class Card extends Reflux.Component {
    constructor() {
        super();
        this.stores = [PrizeStore, LoginStore];
        this.url = 'prizes'
    }

    chooseButton() {
        let card_content = <EditCardContent
            id={this.props.id}
            key={this.props.id}
            name={this.props.name}
            description={this.props.description}
            cost={this.props.cost}
        />;

        if (this.props.emptyTask) {
            card_content = <CreateCardContent
                name={'Введите заголовок'}
                description={'Введите описание'}
            />;
        }

        this.setState({
            card_content: card_content
        });
    }

    componentDidMount() {
        this.chooseButton();
        if (this.state.isParent) {
            this.setState({
                button: <button onClick={PrizeAction.deletePrize}
                                className="waves-effect waves-light btn red darken-3"
                                data-id={this.props.id}>
                    <i className="material-icons">delete</i>
                </button>
            });
        } else {
            this.setState({
            //     button: <button onClick={PrizeAction.editPrize}
            //                     className="waves-effect waves-light btn green darken-1"
            //                     data-id={this.props.id}>
            //         <i className="material-icons">offline_pin</i>
            //     </button>,
                progressbar: <div className="col s12 progress card__progressbar">
                    <div className="determinate" style={{width: `${this.state.user.balance/this.props.cost*100}%`}}></div>
                 </div>
             });
        }
    }

    render() {
        return (
            <div>
                <div className="col s12 m6 card_position">
                    {this.state.progressbar}
                    <div className="determinate" style={{width: "70%"}}></div>
                    <div className="card sticky-action" style={{overflow: 'hidden'}}>
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator" src='http://lorempixel.com/400/600/'/>
                        </div>
                        <div className="card-content">
                        <span className="card-title activator grey-text text-darken-4">
                            {this.props.name}
                            <i className="material-icons right">more_vert</i>
                        </span>
                        </div>

                        <div className="card-action">
                            {this.state.button}
                        </div>

                        <div className="card-reveal" style={{display: 'none', transform: "translateY(0px)"}}>
                        <span className="card-title grey-text text-darken-4">
                            {this.props.name}
                            <i className="material-icons right">close</i>
                        </span>
                            <div>
                                {this.state.card_content}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}