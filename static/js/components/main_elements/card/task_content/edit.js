import React from 'react'
import Reflux from 'reflux';

import TaskStore from "./../../../stores/task"
import TaskAction from './../../../actions/task/task';

export default class EditCardContent extends Reflux.Component {
    constructor() {
        super();
        this.store = TaskStore;
        this.url = 'tasktype'
    }

    render() {
        return (
            <div>
                <p data-change="description" contentEditable="true">{this.props.taskDecription}</p>
                <p>Вознаграждение:
                    <span data-change="cost" contentEditable="true">
                        {`     ${this.props.revard}`}
                    </span>
                </p>
                <button data-id={this.props.id} className="waves-effect waves-light btn"
                        onClick={TaskAction.changeTask}>Изменить
                </button>
            </div>
        )
    }
}