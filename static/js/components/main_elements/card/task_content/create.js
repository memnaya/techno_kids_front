import React from 'react';
import Reflux from 'reflux';

import TaskStore from "./../../../stores/task";
import TaskAction from './../../../actions/task/task';

export default class CreateCardContent extends Reflux.Component {
    constructor() {
        super();
        this.store = TaskStore;
    }

    render() {
        return (
            <div>
                <p data-change="title" contentEditable="true">{this.props.title}</p>
                <p data-change="description" contentEditable="true">{this.props.taskDecription}</p>
                <p >Вознаграждение:
                    <span data-change="cost" contentEditable="true">
                        {`    введите цифру`}
                    </span>
                </p>
                <button className="waves-effect waves-light btn"
                        onClick={TaskAction.saveNewTask}>Создать
                </button>
            </div>
        )
    }
}