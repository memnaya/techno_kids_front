import React from "react";
import Reflux from 'reflux';
import ChildAction from './../../actions/child/child';
import ChildStore from './../../stores/child';


export default class ChildCard extends Reflux.Component {
    constructor() {
        super();
        this.store = ChildStore;
    }

    handleCardStyle(data, button) {
        if (data.status === 0) {

            button.removeClass('red lighten-2');
            button.find('i').text('close');
            button.parent().parent().removeClass('teal');

        } else if (data.status === 1) {

            button.addClass('red lighten-2');
            button.find('i').text('replay');
            button.parent().parent().addClass('teal');
            button.parent().parent().parent().find('.card__alert__button').hide();
        }
    }

    styleCard() {
        let button = $(`a[data-id=${this.props.id}]`).first();
        if (this.props.status == 0) {
            this.handleCardStyle(this.props, button);
        } else if (this.props.status == 1) {
            this.handleCardStyle(this.props, button);
        }
    }

    componentDidMount() {
        this.styleCard();
    }

    render() {

        return (
            <div className="col s12 m4">
                <div className="card">
                    <div className="card-image">
                        <img src="http://lorempixel.com/400/600/" />
                        <span className="card-title">{this.props.title}</span>
                        <a data-id={this.props.id} onClick={ChildAction.closeTask} className="btn-floating halfway-fab waves-effect waves-light red"><i className="material-icons">close</i></a>
                    </div>
                    <div className="card-content">
                        <p>{this.props.description}</p>
                    </div>
                </div>
            </div>
        )
    }
}
