import React from "react";
import Reflux from 'reflux';

import FamilyStore from './../../stores/family'
import FamilyAction from './../../actions/family/family'

import Card from './../../main_elements/card/family_card'

require('./../../../../css/content/__family/family.css');

export default class FamilySet extends Reflux.Component {
    constructor() {
        super();
        this.store = FamilyStore;
        this.url = 'family';
    }

    componentDidMount() {
        let cards = [];
        this.props.members.forEach((el)=>{
            cards.push(<Card
                id={el.id}
                key={el.id}
                email={el.user.email}
                first_name={el.user.first_name}
                last_name={el.user.last_name}
                role_name={el.role.name}
                gender={el.gender}
                balance={el.balance}
                avatar={el.avatar}
                birth_date={el.birth_date}
            />)
        });
        this.setState({
            cards: cards
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.new_card && this.state.user_type === this.props.id) {
            nextState.cards.unshift(nextState.new_card);
            nextState.empty_card = 'empty';
            nextState.new_card = undefined;
        }
        return true;
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.empty_card === 'empty') {
            this.setState({
                empty_card: undefined,
                new_card: undefined
            });
        }
    }



    render() {

        return (
            <div className="container__row_parent">
                <h4>
                    {this.props.memberName}
                </h4>
                <a onClick={FamilyAction.addMember}
                   data-id={this.props.id}
                   className="container__row__button_add btn-floating btn-large waves-effect waves-light red">
                    <i className="material-icons">add</i>
                </a>
                <div className="row">
                    {this.state.user_type === this.props.id && this.state.empty_card != 'empty' ? this.state.empty_card : '' } {this.state.cards}
                </div>
            </div>
        )
    }
}