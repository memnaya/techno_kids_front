import React from "react";
import Reflux from 'reflux';

import TaskStore from './../../stores/task'
import TaskAction from './../../actions/task/task'

import Card from './../../main_elements/card/task_card'

require('./../../../../css/content/__family/family.css');

export default class TaskSet extends Reflux.Component {
    constructor() {
        super();
        this.store = TaskStore;
        this.url = 'tasktype'
    }

    componentDidMount() {
        let cards = [];
        this.props.childTasks.forEach((el)=>{
            cards.push(<Card
                id={el.id}
                key={el.id}
                picture={el.picture}
                status={el.status}
                typeId={el.type_info.id}
                revard={el.cost}
                taskName={el.title}
                taskDecription={el.description}
            />)
        });
        this.setState({
            cards: cards
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.new_card && this.state.task_set_id === this.props.id) {
            nextState.cards.unshift(nextState.new_card);
            nextState.empty_card = 'empty';
            nextState.new_card = undefined;
        }
        return true;
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.empty_card === 'empty') {
            this.setState({
                empty_card: undefined,
                new_card: undefined
            });
        }
    }



    render() {

        return (
            <div className="container__row_parent">
                <h4 className="h4_position">
                    <div className="h4__arrow_right">
                    </div>
                    <span>
                        {this.props.childName}
                    </span>
                </h4>
                <a onClick={TaskAction.addTask}
                   data-id={this.props.id}
                   className="container__row__button_add btn-floating btn-large waves-effect waves-light red">
                    <i className="material-icons">add</i>
                </a>
                <div className="row">
                    {this.state.task_set_id === this.props.id && this.state.empty_card != 'empty' ? this.state.empty_card : '' } {this.state.cards}
                </div>
            </div>
        )
    }
}