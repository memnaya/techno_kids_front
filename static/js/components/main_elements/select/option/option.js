import React from "react";
import Utils from  './../../../../lib/utils'

export default class Option extends React.Component {
    render() {
        return (
            <option value={this.props.id}
                    data-icon={`${Utils.serverInfo.host}${this.props.picture}`}
                    className="left circle">
                {this.props.name}
            </option>
        )
    }
}