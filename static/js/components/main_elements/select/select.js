import React from "react";
import Reflux from 'reflux'

import Option from './option/option'

export default class Select extends Reflux.Component {
    constructor() {
        super();
        this.state = {};
        this.url = 'tasktype'
    }
    componentWillMount() {
        let options = [];
        this.props.options.forEach((el)=>{
            options.push(<Option key={el.id} id={el.id} picture={el.picture} name={el.name}/>);
        });
        this.setState({
            options: options
        });
    }

    componentDidMount() {
        $(`select[data-select=${this.props.selected}]`).val(this.props.selected).material_select();
    }

    render() {
        return (
            <select data-change={this.props.change} data-select={this.props.selected} className="icons">
                <option value="" disabled selected>Выберите тип</option>
                {this.state.options}
            </select>
        )
    }
}