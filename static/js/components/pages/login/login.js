import React from "react";
import Reflux from 'reflux';
import LoginAction from './../../actions/login/login';
import LoginStore from './../../stores/login';

require('./../../../../css/content/__login_form/login_form.css');

export default class Login extends Reflux.Component {
    constructor() {
        super();
        this.store = LoginStore;
    }

    render() {
        return (
            <div className="container">
                <div>
                    <div className="col s12 offset-m4 m4">
                        <div className="content__login_margin">
                            <div className="card z-depth-5">
                            <div className="card-content">
                            <span className="card-title grey-text text-darken-3">Wish</span>
                            <form className="content__login_form" action="#">
                            
                                <div className="row">
                                    <label htmlFor="emailInput">Имя пользователя</label>
                                    <input className="u-full-width" type="email" placeholder="Ваш e-mail" id="emailInput"/>
                                </div>
                                <div className="row">
                                    <label htmlFor="passwordInput">Пароль</label>
                                    <input className="u-full-width" type="password" placeholder="Ваш пароль" id="passwordInput"/>
                                </div>
                                <div className="row">
                                    <input onClick={LoginAction.login} className="waves-effect waves-light btn" type="submit" value="Войти"/>
                                </div>
                            </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}