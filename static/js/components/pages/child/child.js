import React from "react";
import Reflux from 'reflux';

import ChildCard from './../../main_elements/card/child_card';

import LoginStore from './../../stores/login';

import CookieManager from  './../../../lib/cookie'
import Utils from  './../../../lib/utils'

require('./../../../../css/content/__child/child.css');

export default class Child extends Reflux.Component {
    constructor() {
        super();
        this.url = 'tasks';
        this.store = LoginStore;
        this.token = CookieManager.getCookie('token');
    }

    componentWilMount() {
        
    }

    componentDidMount() {

        Utils.sendRequest({
            path: this.url,
            token: this.token
        }).success((data)=> {
            let card_tasks = [];
            data[0].tasks.forEach((el)=> {
                card_tasks.push(<ChildCard
                    id={el.id}
                    key={el.id}
                    title={el.title}
                    description={el.description}
                    status={el.status}
                />)
            });
            this.setState({
                card_tasks: card_tasks
            });
        })
    }

    render() {

        return (

            <div className="container">
                <h2>Твои задания</h2>
                <div className="row">
                    {this.state.card_tasks}
                </div>
            </div>
        )
    }
}