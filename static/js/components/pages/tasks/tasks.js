import React from "react";
import Reflux from 'reflux';

import LoginStore from './../../stores/login';
import TaskSet from './../../main_elements/task_set/task_set'

import CookieManager from  './../../../lib/cookie'
import Utils from  './../../../lib/utils'

export default class Tasks extends Reflux.Component {
    constructor() {
        super();
        this.url = 'tasks';
        this.store = LoginStore;
    }

    componentDidMount() {
        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: this.url,
            token: token
        }).success((data)=> {
            let child_and_tasks = [];
            data.forEach((el)=> {
                child_and_tasks.push(<TaskSet id={el.id} key={el.id} childName={el.name} childTasks={el.tasks}/>)
            });
            this.setState({
                child_and_tasks: child_and_tasks
            });
        })
    }

    render() {
        return (

            <div className="container">
                <h2>
                    Задачи
                </h2>
                <div className="row">
                    {this.state.child_and_tasks}
                </div>
            </div>
        )
    }
}