import React from "react";
import Reflux from 'reflux';

import PrizeStore from './../../stores/prize';
import LoginStore from './../../stores/login';

import PrizeAction from './../../actions/prize/prize';

import Card from './../../main_elements/card/prize_card'

import CookieManager from  './../../../lib/cookie'
import Utils from  './../../../lib/utils'

require('./../../../../css/content/__family/family.css');

export default class Prizes extends Reflux.Component {
    constructor() {
        super();
        this.stores = [PrizeStore, LoginStore];
        this.url = 'prizes';
    }

    componentDidMount() {
        let cards = [];
        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: this.url,
            token: token,
        }).success((data)=> {
            data.forEach((el)=> {
                cards.push(
                    <Card
                        id={el.id}
                        key={el.id}
                        name={el.name}
                        description={el.description}
                        cost={el.cost}
                    />)
            });
            this.setState({
                cards: cards
            });
        }).error((error)=> {
            swal("Error", error.responseText, "error")
        });

        if (this.state.isParent) {
            this.setState({
                addButton: <a onClick={PrizeAction.addPrize}
                              className="container__row__button_add btn-floating btn-large waves-effect waves-light red">
                    <i className="material-icons">add</i>
                </a>
            })
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.new_card) {
            nextState.cards.unshift(nextState.new_card);
            nextState.empty_card = 'empty';
            nextState.new_card = undefined;
        }
        return true;
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.empty_card === 'empty') {
            this.setState({
                empty_card: undefined,
                new_card: undefined
            });
        }
    }

    render() {

        return (

            <div className="container container__row_parent">
                <h2>
                    Призы
                </h2>
                {this.state.addButton}
                <div className="row">
                    {this.state.empty_card != 'empty' ? this.state.empty_card : '' } {this.state.cards}
                </div>
            </div>
        )
    }
}