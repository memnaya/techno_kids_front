import React from "react";
import Reflux from 'reflux';

import LoginStore from './../../stores/login';
import DiaryStore from './../../stores/diary';

require('./../../../../css/content/__family/family.css');

export default class Statistic extends Reflux.Component {
    constructor() {
        super();
        this.stores = [LoginStore, DiaryStore];
    }

    componentDidMount() {

        // Request('https://e97475c8.ngrok.io/diary/', function (error, response, body) {
        //     console.log('error:', error); // Print the error if one occurred
        //     console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        //     console.log('body:', body); // Print the HTML for the Google homepage.
        // });

    }

    render() {
        return (
            <div className="container container__row_parent">
                <h2>
                    Статистика
                </h2>
                {this.state.addButton}
                <div className="row">
                    <ul className="collection with-header">
                        {this.state.diary}
                    </ul>
                </div>
            </div>
        )
    }
}