import React from "react";
import Reflux from 'reflux';


import LoginAction from './../../actions/login/login';
import LoginStore from './../../stores/login';

import Select from './../../main_elements/select/select'

import Utils from  './../../../lib/utils'
import CookieManager from  './../../../lib/cookie'

require('./../../../../css/content/__login_form/login_form.css');

export default class Registration extends Reflux.Component {
    constructor() {
        super();
        this.store = LoginStore;
    }

    componentDidMount() {
        $('.datepicker').pickadate({
            container: ".container",
            selectMonths: true,
            selectYears: 15,
            format: 'yyyy-mm-dd'
        });

        let male_data = [{
            name: " М",
            id: 1
        }, {
            name: "Ж",
            id: 0
        }];

        this.setState({
            select: <Select change="role" selected={0} options={male_data}/>
        });
    }

    render() {
        return (
            <div className="container">
                <div>
                    <div className="col s12 offset-m4 m4">
                        <div className="content__login_margin">
                            <form className="content__registration_form" action="#">
                                <div className="row">
                                    <label htmlFor="emailInput">Email</label>
                                    <input className="u-full-width" type="email" placeholder="lala@gmail.com"
                                           id="emailInput"/>
                                </div>
                                <div className="row">
                                    <label htmlFor="firstName">Имя</label>
                                    <input className="u-full-width" type="email" placeholder="Ваше имя"
                                           id="firstName"/>
                                </div>
                                <div className="row">
                                    <label htmlFor="lastName">Фамилия</label>
                                    <input className="u-full-width" type="email" placeholder="Ваша фамилия"
                                           id="lastName"/>
                                </div>
                                <div className="row">
                                    <label htmlFor="passwordInput">Пароль</label>
                                    <input className="u-full-width" type="password" placeholder="Секретный пароль"
                                           id="passwordInput"/>
                                </div>
                                <div className="row">
                                    <div className="input-field">
                                        <input data-change="birth_date" type="date" className="datepicker" />
                                    </div>
                                </div>
                                <div className="row">
                                    {this.state.select}
                                </div>
                                <div className="row">
                                    <input onClick={LoginAction.registration} className="waves-effect waves-light btn"
                                           type="submit" value="Submit"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}