import React from "react";
import Reflux from 'reflux';

import FamilyStore from './../../stores/family';
import LoginStore from './../../stores/login';

import FamilySet from './../../main_elements/family_set/family_set'

import CookieManager from  './../../../lib/cookie'
import Utils from  './../../../lib/utils'

require('./../../../../css/content/__family/family.css');

export default class Family extends Reflux.Component {
    constructor() {
        super();
        this.stores = [FamilyStore, LoginStore];
        this.url = 'family';
    }

    componentDidMount() {
        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: this.url,
            token: token
        }).success((data)=> {
            let family = [];
            data.forEach((el)=> {
                family.push(<FamilySet id={el.id} key={el.id} memberName={el.name} members={el.data}/>)
            });
            this.setState({
                family: family
            });
        })
    }

    render() {
        return (
            <div className="container">
                <h2>
                    Семья
                </h2>
                <div className="row">
                    {this.state.family}
                </div>
            </div>
        )
    }
}