import React from "react";
import Reflux from 'reflux';

import LoginStore from './../../stores/login';
import SettingsStore from './../../stores/settings';

import SettingsAction from './../../actions/settings/settings';

import Select from './../../main_elements/select/select'

import CookieManager from  './../../../lib/cookie'
import Utils from  './../../../lib/utils'

require('./../../../../css/content/__settings/settings.css');

export default class Settings extends Reflux.Component {
    constructor() {
        super();
        // !!!!!!!!!!!!!!!!!!!!!
        this.url = '';
        this.stores = [SettingsStore, LoginStore];
    }

    componentDidMount() {
        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: 'roles',
            token: token,

        }).success((data)=> {
            this.setState({
                role_select: <Select change="role" selected={0} options={data}/>,
            });
        });
    }

    render() {

        return (

            <div className="container">
                <h2>
                    Настройки
                </h2>
                <div className="col s12">
                    <div className="row container__row_parent">
                        <a onClick={SettingsAction.addRole}
                           data-id={this.props.id}
                           className="container__row__button_add btn-floating btn-large waves-effect waves-light red">
                            <i className="material-icons">add</i>
                        </a>
                        {this.state.role_select}
                        <div className="col s4 container__row__input">
                            <input type="text" />
                            <a className="btn" onClick={SettingsAction.saveNewRole}>Сохранить</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}