import React from "react";
import Reflux from 'reflux';

import LoginStore from './../../stores/login';
import StatisticStore from './../../stores/statistic';

import ParentStatistic from './../../main_elements/statistic/parent'
import ChildStatistic from './../../main_elements/statistic/child'

import StatisticAction from './../../actions/statistic/statistic';

import CookieManager from  './../../../lib/cookie'
import Utils from  './../../../lib/utils'

require('./../../../../css/content/__family/family.css');

export default class Statistic extends Reflux.Component {
    constructor() {
        super();
        this.stores = [LoginStore, StatisticStore];
        this.url = 'statistic';
    }

    componentDidMount() {
        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: this.url,
            token: token
        }).success((data)=> {
            let statistic, addButton;
            if (this.state.isParent) {
                addButton = <a onClick={StatisticAction.addSin}
                              className="container__row__button_add btn-floating btn-large waves-effect waves-light red">
                    <i className="material-icons">add</i>
                </a>;
                statistic = <ParentStatistic data={data}/>
            } else {
                addButton = '';
                statistic = <ChildStatistic data={data}/>
            }
            this.setState({
                statistic: statistic,
                addButton: addButton
            });
        });
    }

    render() {
        return (
            <div className="container container__row_parent">
                <h2>
                    Статистика
                </h2>
                {this.state.addButton}
                <div className="row">
                    {this.state.statistic}
                </div>
            </div>
        )
    }
}