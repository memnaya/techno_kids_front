import React from 'react';
import Reflux from 'reflux';
import StatisticAction from './../actions/statistic/statistic';
import Select from './../main_elements/select/select'
import CookieManager from  './../../lib/cookie'
import Utils from  './../../lib/utils'

export default class StatisticStore extends Reflux.Store {
    constructor() {
        super();
        this.listenables = StatisticAction;
        this.token = CookieManager.getCookie('token');
        this.url = 'statistic'
    }

    onAddSin() {
        Utils.sendRequest({
            path: 'statistic',
            token: this.token,

        }).success((data)=> {
            this.setState({
                new_sin: <li className="collection-item">
                    <div>
                        <div>
                            <label htmlFor="sin_title">Заголовок проступка</label>
                            <input id="sin_title" type="text"/>
                            <label htmlFor="sin_desc">Описание проступка</label>
                            <input id="sin_desc" type="text"/>
                            <Select change="child" selected={0} options={data.sins}/>
                        </div>
                        <button onClick = {StatisticAction.saveSin} className="button_margin btn">Добавить</button>
                    </div>
                </li>

            });
        });
    }

    onSaveSin(e) {
        let data = new FormData();
        data.append("title", $('#sin_title').val());
        data.append("description", $('#sin_desc').val());
        data.append("submissive", parseInt($('[data-change=child]').val()));

        let xhr = new XMLHttpRequest();

        let self = this;

        xhr.addEventListener("readystatechange", function () {

            if (this.readyState === 4) {
                var response_data = JSON.parse(xhr.response);
                self.setState({
                    new_sin_row: <li className="collection-item"><div>{response_data.submissive_info.user.first_name} - {response_data.title}<a className="secondary-content bad"><i className="material-icons">not_interested</i></a></div></li>,
                })
            }
        });

        xhr.open("POST", "http://78.155.219.128:8888/sins/");
        xhr.setRequestHeader("authorization", `Token ${this.token}`);

        xhr.send(data);

        // Utils.sendRequest({
        //     path: 'sins',
        //     token: this.token,
        //     type: 'POST',
        //     data: data
        // })
        //     .success((data)=> {
        //         debugger;
        //         this.setState({
        //             new_sin: null,
        //             sins: data
        //         })
        //     })
        //     .error((error)=> {
        //         swal("Error", error.responseText, "error")
        //     });
    }
}