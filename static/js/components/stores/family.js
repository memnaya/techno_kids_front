import React from "react";
import Reflux from 'reflux';
import FamilyAction from './../actions/family/family';

import Card from './../main_elements/card/family_card'

import CookieManager from  './../../lib/cookie'
import Utils from  './../../lib/utils'

export default class FamilyStore extends Reflux.Store {
    constructor() {
        super();
        this.listenables = FamilyAction;
        this.token = CookieManager.getCookie('token');
        this.url = 'profiles';
    }

    onAddMember(e) {
        let user_type = parseInt($(e.currentTarget).data('id'));

        this.setState({
            user_type: user_type,
            empty_card: <Card
                title={"Нажми на меня"}
                emptyTask={true}
            />
        });
    }

    onChangeMember(e) {
        let member_id = $(e.target).data('id');
        let card = $(e.target).parent();

        let data = {
            user: {
                id: member_id,
                first_name: card.find('[data-change=first_name]').text(),
                last_name: card.find('[data-change=last_name]').text(),
            },
            birth_date: card.find('[data-change=birth_date]').pickadate('picker').get()
        };

        Utils.sendRequest({
            path: this.url,
            token: this.token,
            id: member_id,
            type: 'PUT',
            data: data
        }).error((error)=> {
            swal("Error", error.responseText, "error")
        });
    }

    onCreateMember(e) {
        let card = $(e.currentTarget).parents().eq(3);
        let type = card.parent().prev().data('id');

        let data = JSON.stringify({
            "user": {
                "email": card.find('[data-change=email]').text(),
                "first_name": card.find('[data-change=first_name]').text(),
                "last_name": card.find('[data-change=last_name]').text(),
                "password": card.find('[data-change=password]').text()
            },
            "role": {
                "id": parseInt(card.find('[data-change=role]').val())
            },
            "type": type,
            "gender": parseInt(card.find('[data-change=gender]').val()),
            "birth_date": card.find('[data-change=birth_date]').pickadate('picker').get(),
            "balance": 0
        });

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "http://78.155.219.128:8888/profiles/");
        xhr.setRequestHeader("authorization", `Token ${this.token}`);
        xhr.setRequestHeader("content-type", "application/json");
        xhr.send(data);

        var self = this;

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status >= 200)
                var response_data = JSON.parse(xhr.response);

                let new_task = <Card
                    id={response_data.id}
                    key={response_data.id}
                    email={response_data.user.email}
                    first_name={response_data.user.first_name}
                    last_name={response_data.user.last_name}
                    role_name={response_data.role.name}
                    gender={response_data.gender}
                    balance={response_data.balance}
                    avatar={response_data.avatar}
                    birth_date={response_data.birth_date}
                />;

                self.setState({
                    new_card: new_task
                });
            } else {
                swal("Error", error.responseText, "error");
            }
        };
    }

    deleteRequest(member_id, button) {
        Utils.sendRequest({
            path: this.url,
            token: this.token,
            id: member_id,
            type: 'DELETE',
        })
            .success(()=> {
                button.parent().parent().empty();
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }

    onDeleteMember(e) {
        let button = $(e.currentTarget);
        let member_id = button.data('id');

        Utils.deleteAlert(this.deleteRequest.bind(this, member_id, button))
    }
}