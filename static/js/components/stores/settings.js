import React from "react";
import Reflux from 'reflux';

import SettingsAction from './../actions/settings/settings';

import Select from './../main_elements/select/select'

import CookieManager from  './../../lib/cookie'
import Utils from  './../../lib/utils'

export default class SettingsStore extends Reflux.Store {
    constructor() {
        super();
        this.listenables = SettingsAction;
        this.token = CookieManager.getCookie('token');
    }

    onAddRole(e) {
        $(e.currentTarget).next().next().show()
    }

    onSaveNewRole(e) {
        let role_input = $(e.currentTarget).prev();

        let data = {
            name: role_input.val()
        };

        Utils.sendRequest({
            path: 'roles',
            token: this.token,
            data: data,
            type: "POST"
        })
            .success(()=>{
                swal("Успешно добавлено", "success");

                role_input.parent().hide();

            //    UPDATE SELECTOR

            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }
}