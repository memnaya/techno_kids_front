import React from 'react'
import Reflux from 'reflux';
import TaskAction from './../actions/task/task';

import Card from './../main_elements/card/task_card'

import CookieManager from  './../../lib/cookie'
import Utils from  './../../lib/utils'

export default class TaskStore extends Reflux.Store {
    constructor() {
        super();
        this.listenables = TaskAction;
        this.url = 'tasks';
        this.token = CookieManager.getCookie('token')
    }

    getFormData(task_id) {
        return Utils.sendRequest({
            path: this.url,
            id: task_id,
            token: this.token
        })
    }

    handleCardStyle(data, button) {
        if (data.status === 0) {

            button.removeClass('red lighten-2');
            button.find('i').text('done');
            button.parent().parent().removeClass('teal');

        } else if (data.status === 1) {

            button.parent().parent().parent().find('.card__alert__button').show();

        } else if (data.status === 2) {

            button.addClass('red lighten-2');
            button.find('i').text('replay');
            button.parent().parent().addClass('teal');
            button.parent().parent().parent().find('.card__alert__button').hide();

        }
    }

    onChangeTaskStatus(e) {
        let button = $(e.currentTarget);
        let task_id = button.data('id');

        let data = {
            status: 0
        };

        if (button.find('i').text() === 'done') {
            data = {
                status: 2
            };

        }

        Utils.sendRequest({
            path: this.url,
            token: this.token,
            id: task_id,
            type: 'PATCH',
            data: data
        })
            .success((data)=> {
                this.handleCardStyle(data, button);
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }

    onChangeTask(e) {
        let task_id = $(e.target).data('id');
        let card = $(e.target).parent();

        this.getFormData(task_id).success((data)=> {
            // какая-то хуйня
            delete data.customer_info;
            delete data.supplier_info;
            delete data.type_info;
            delete data.status;
            delete data.picture;

            this.setState({
                selected: data.type
            });

            data.type = parseInt(card.find('select').val());
            data.description = card.find('[data-change=description]').text();
            data.cost = parseInt(card.find('[data-change=cost]').text());

            Utils.sendRequest({
                path: this.url,
                token: this.token,
                id: task_id,
                type: 'PUT',
                data: data
            }).error((error)=> {
                swal("Error", error.responseText, "error")
            });
        });
    }

    deleteRequest(task_id, button) {
        Utils.sendRequest({
            path: this.url,
            token: this.token,
            id: task_id,
            type: 'DELETE',
        })
            .success(()=> {
                button.parent().parent().empty();
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }

    onDeleteTask(e) {
        let button = $(e.currentTarget);
        let task_id = button.data('id');

        Utils.deleteAlert(this.deleteRequest.bind(this, task_id, button));
    }

    onAddTask(e) {
        let task_id = parseInt($(e.currentTarget).data('id'));

        this.setState({
            task_set_id: task_id,
            empty_card: <Card
                typeId={0}
                taskDecription={"Введите описание"}
                taskName={"Нажми на меня"}
                emptyTask={true}
            />
        });
    }

    onSaveNewTask(e) {
        let card = $($(e.currentTarget).parents()[3]);

        // let data = {
        //     title: card.find('[data-change=title]').text(),
        //     type: parseInt(card.find('select').val()),
        //     description: card.find('[data-change=description]').text(),
        //     cost: parseInt(card.find('[data-change=cost]').text()),
        //     supplier: $(card.parents()[5]).find('.container__row__button_add').data('id'),
        // };

        let data = JSON.stringify({
            "title": card.find('[data-change=title]').text(),
            "description": card.find('[data-change=description]').text(),
            "type": parseInt(card.find('select').val()),
            "supplier": $(card.parents()[5]).find('.container__row__button_add').data('id'),
            "cost": parseInt(card.find('[data-change=cost]').text())

        });

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "http://78.155.219.128:8888/tasks/");
        xhr.setRequestHeader("authorization", `Token ${ this.token }`);
        xhr.setRequestHeader("content-type", "application/json");

        xhr.send(data);

        let self = this;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {

                var response_data = JSON.parse(xhr.response);

                let new_task = <Card
                    id={response_data.id}
                    key={response_data.id}
                    picture={response_data.picture}
                    status={response_data.status}
                    typeId={response_data.type_info.id}
                    revard={response_data.cost}
                    taskName={response_data.title}
                    taskDecription={response_data.description}
                />;
                self.setState({
                    new_card: new_task
                });
            }
        });

        // Utils.sendRequest({
        //     path: this.url,
        //     token: this.token,
        //     type: 'POST',
        //     data: data
        // })
        //     .success((data)=> {
        //         let new_task = <Card
        //             id={data.id}
        //             key={data.id}
        //             picture={data.picture}
        //             status={data.status}
        //             typeId={data.type_info.id}
        //             revard={data.cost}
        //             taskName={data.title}
        //             taskDecription={data.description}
        //         />;
        //         this.setState({
        //             new_card: new_task
        //         });
        //     })
        //     .error((error)=> {
        //         debugger;
        //         swal("Error", error.responseText, "error")
        //     });
    }
}