import Reflux from 'reflux';
import LoginAction from './../actions/login/login';
import CookieManager from  './../../lib/cookie'
import Utils from  './../../lib/utils'
import {browserHistory} from 'react-router'

export default class LoginStore extends Reflux.Store {
    constructor() {
        super();
        this.url = 'me';
        this.listenables = LoginAction;

        let token = CookieManager.getCookie('token');

        if (CookieManager.getCookie('token')) {
            this.setUserInfo(token);
        } else {
            browserHistory.push('/login');
            CookieManager.deleteCookie('token');
        }
    }

    setUserInfo(token) {
        Utils.sendRequest({
            path: this.url,
            token: token
        }).success((data)=> {
            let user_info = data;
            let isParent = user_info.type == 0;

            this.setState({
                isParent: isParent,
                user: user_info
            });
            if (this.state.user.type === 1) {
                browserHistory.push('/child');
            } else if (this.state.user.type === 0) {
                // TODO: сделать нормальный редирект для взрослых
                browserHistory.push('/tasks');
            }
        }).error((error)=> {
            swal("Error", error.responseText, "error");
            CookieManager.deleteCookie('token');
            browserHistory.push('/login');
        });
    }

    onLogin(e) {
        e.preventDefault();
        let data = {
            email: $(e.target).parents().find('#emailInput').val(),
            password: $(e.target).parents().find('#passwordInput').val()
        };
        Utils.sendRequest({
            path: 'login',
            data: data,
            type: "POST"
        })
            .success((data)=> {
                CookieManager.setCookie('token', data.token);
                this.setUserInfo(data.token)
            })
            .error((error)=> {
                swal("Error", error.responseText, "error");
                CookieManager.deleteCookie('token');
            });
    }

    onLogout(e) {
        let token = CookieManager.getCookie('token');

        Utils.sendRequest({
            path: 'logout',
            token: token,
            type: "POST"
        })
            .success(()=> {
                CookieManager.deleteCookie('token');
                this.setState({
                    showNavbar: false,
                    user: undefined
                });
                // browserHistory.push('/login')
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }

    onRegistration(e) {
        let form = $(e.currentTarget).parents().eq(2);

        let data = {
            user: {
                email: $('#emailInput').val(),
                first_name: $('#firstName').val(),
                last_name: $('#lastName').val(),
                password: $('#passwordInput').val()
            },
            type: 1,
            gender: parseInt(form.find('[data-change=gender]').val()),
            birth_date: form.find('[data-change=birth_date]').pickadate('picker').get(),
            balance: 0
        };

        Utils.sendRequest({
            path: this.url,
            token: this.token,
            type: 'POST',
            data: data
        })
            .success((data)=> {
                // СДЕЛАТЬ СЕТ НА ЮЗЕРА И РЕДИРЕКТЫ
                // this.setState({
                //     new_card: new_task
                // });
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }
}