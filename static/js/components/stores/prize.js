import React from "react";
import Reflux from 'reflux';

import PrizeAction from './../actions/prize/prize';

import Card from './../main_elements/card/prize_card'

import CookieManager from  './../../lib/cookie'
import Utils from  './../../lib/utils'

export default class PrizeStore extends Reflux.Store {
    constructor() {
        super();
        this.listenables = PrizeAction;
        this.token = CookieManager.getCookie('token');
        this.url = 'prizes';
    }

    onAddPrize(e) {
        this.setState({
            empty_card: <Card
                name={"Нажми на меня"}
                emptyTask={true}
            />
        });
    }

    onEditPrize(e) {
        let member_id = $(e.target).data('id');
        let card = $(e.target).parent();

        let data = {
            name: card.find('[data-change=name]').text(),
            description: card.find('[data-change=description]').text(),
            cost: parseInt(card.find('[data-change=cost]').text()),
        };

        Utils.sendRequest({
            path: this.url,
            token: this.token,
            id: member_id,
            type: 'PUT',
            data: data
        }).error((error)=> {
            swal("Error", error.responseText, "error")
        });
    }

    onCreatePrize(e) {
        let card = $($(e.currentTarget).parents()[3]);

        let data = {
            name: card.find('[data-change=name]').text(),
            description: card.find('[data-change=description]').text(),
            cost: parseInt(card.find('[data-change=cost]').text()),
        };

        Utils.sendRequest({
            path: this.url,
            token: this.token,
            type: 'POST',
            data: data
        })
            .success((data)=> {
                let new_task = <Card
                    id={data.id}
                    key={data.id}
                    name={data.name}
                    description={data.description}
                    cost={data.cost}
                />;
                this.setState({
                    new_card: new_task
                });
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }

    deleteRequest(member_id, button) {
        Utils.sendRequest({
            path: this.url,
            token: this.token,
            id: member_id,
            type: 'DELETE',
        })
            .success(()=> {
                button.parent().parent().empty();
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }

    onDeletePrize(e) {
        let button = $(e.currentTarget);
        let member_id = button.data('id');

        Utils.deleteAlert(this.deleteRequest.bind(this, member_id, button))
    }
}