import React from 'react';
import Reflux from 'reflux';
import ChildAction from './../actions/child/child';

import CookieManager from  './../../lib/cookie'
import Utils from  './../../lib/utils'

export default class LoginStore extends Reflux.Store {
    constructor() {
        super();
        this.listenables = ChildAction;
        this.token = CookieManager.getCookie('token');
        this.url = 'tasks'
    }

    handleCardStyle(data, button) {
        if (data.status === 0) {

            button.removeClass('red lighten-2');
            button.find('i').text('close');
            button.parent().parent().removeClass('teal');

        } else if (data.status === 1) {

            button.addClass('red lighten-2');
            button.find('i').text('replay');
            button.parent().parent().addClass('teal');
            button.parent().parent().parent().find('.card__alert__button').hide();
        }
    }

    onCloseTask(e) {
        let button = $(e.currentTarget);
        let task_id = button.data('id');

        let data = {
            status: 0
        };

        if (button.find('i').text() === 'close') {
            data = {
                status: 1
            };

        }

        Utils.sendRequest({
            path: this.url,
            token: this.token,
            id: task_id,
            type: 'PATCH',
            data: data
        })
            .success((data)=> {
                this.handleCardStyle(data, button);
            })
            .error((error)=> {
                swal("Error", error.responseText, "error")
            });
    }
}