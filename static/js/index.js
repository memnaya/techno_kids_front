import React from "react";
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from "react-router"

// main app
import App from './components/main_elements/app'

// pages
import Home from './components/pages/home/home'
import Login from './components/pages/login/login'
import Registration from './components/pages/registration/registration'
import Tasks from './components/pages/tasks/tasks'
import Family from  './components/pages/family/family'
import Prises from './components/pages/prizes/prizes'
import Settings from './components/pages/settings/settings'
import Statistic from './components/pages/statistic/statistic'
import Diary from './components/pages/diary/diary'

import Child from './components/pages/child/child'

import NotFound from './components/pages/not_found/not_found'


ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={App}>

            <IndexRoute component={Home} />

            <Route path="login" component={Login} />
            <Route path="registration" component={Registration} />
            <Route path="tasks" component={Tasks} />
            <Route path="family" component={Family} />
            <Route path="prises" component={Prises} />
            <Route path="statistic" component={Statistic} />
            <Route path="settings" component={Settings} />
            <Route path="diary" component={Diary} />

            <Route path="child" component={Child} />

            <Route path='*' component={NotFound} />
        </Route>
    </Router>,
    document.getElementById("body")
);